<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_055708_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'body' => $this->text(),
            'category' => $this->string(),
            'auther' => $this->string(),
            'status' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
