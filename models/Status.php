<?php

namespace app\models;

use Yii;

use yii\helpers\ArrayHelper;
use app\models\status;





/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $status_name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
        ];
    }
	
	
			public static function getStatus()
	{
		$status = ArrayHelper::
					map(self::find()->all(), 'id', 'status_name');
		return $status;						
	}
	
			public static function getStatusesWithAllStatuses()
			{
			$allStatuses = self::getStatus();
			$allStatuses[null] = 'All Statuses';
			$allStatuses = array_reverse ( $allStatuses, true );
			return $allStatuses;	
}
	
	
	
	
	
}
