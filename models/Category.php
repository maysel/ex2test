<?php

namespace app\models;

use Yii;

use yii\helpers\ArrayHelper;
use app\models\Category;



/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $category_name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
        ];
    }
	
	
		public static function getCategory()
	{
		$category = ArrayHelper::
					map(self::find()->all(), 'id', 'category_name');
		return $category;						
	}
	
	
	
			public static function getCategoriesWithAllCategories()
			{
			$allCategories = self::getCategory();
			$allCategories[null] = 'All Categories';
			$allCategories = array_reverse ( $allCategories, true );
			return $allCategories;	
			}
	
	
	
	
	
}
